# Simple Lane Detector

This is the first project at this Nanodegree Program (NP).  It consists of highlighting the road lanes lines at the images.

The code is in a single [Jupyter Notebook](./main.ipynb) and also vailable in an [HTML document](./main.html).

#### At this project the goal is to make a pipeline that finds lane lines on the road. The output is an image with the lane line highlighted on the road. Either images or video can be input to test the pipeline. 

<a href="./test_videos_output/challenge.mp4" title="Link Title"><img src="./print.png" alt="Alternate Text" /></a>

</br>

The video above is an example of the generated output by the pipeline to the challenge video. Note that the test videos and images are in [test_videos](./test_videos) and [test_images](./test_images) folders respectively. Thus, the results are in the [test_videos_output](./test_videos_output) and [test_images_output](./test_images_output). There are still folders with '_var' at the end, those folders contains the output when drawing the variance lines.

---
### 1. Pipeline description
Briefly, the pipeline consist of filtering operations to make a image with almost just the lanes on it. Then, Canny Edge algorithm followed by HoughTransform to detect the lines and finally draw them.

My pipeline consists of 5 steps:</br>
1. [Filtering white and yellow colors](#filtering-white-and-yellow-colors)</br>
2. [Canny Edge detection](#canny-edge-detection)</br>
3. [Mask the region of interest](#mask-the-region-of-interest)</br>
4. [Hough lines detection](#gaussian-blurring)</br>
5. [Drawing the lines](#drawing-the-lines)</br>
</br>

The pipeline is defined in `process_image` function which receives an image to detect and outputs an annotaded image highlighting the lane lines. This function uses other functions called Helper Functions that are most OpenCV wrapper to improve code readabilty. An example of the process in the pipeline is shown bellow:


<img src="./pipeline.png">  

---
#### Filtering white and yellow colors
Although color filtering is not robust, it was necessary to separate the lane line borders. Doing color thresholding in just grayscaled images are less robust than using hole RGB or HSV colorspace, in fact, to the challenge video segmenting on grayscaled images is not enought.

So, to avoid overfitting the colors range presented in our dataset the threshold parameters were set with a slack making it more robust to color changes.

```python
white = cv2.inRange(image, (200, 200, 200), (255, 255, 255))
yellow = cv2.inRange(image, (180, 180, 0), (255, 255, 150))
binary = white + yellow
```

Thus, since it is already binary no need to convert to gray scale or do gaussian filter berfore applying the Canny Edge algorithm.

---
#### Canny Edge detection

Canny Edge is the most famous edge detection algorithm. To use it I simply use one of the helpers functions in this way:

```python
low_threshold = 50
high_threshold = 150
edges = canny(binary, low_threshold, high_threshold)
```

---
#### Region of interest definition
To filter out unnecessary objects in the image, the region of interest is defined. Such mask (here it's trapezoid) is then applied to the working image using one of the helpers functions in this way:.
```python
imshape = image.shape
vertices = np.array([[(imshape[1]*0.07,imshape[0]),
                        (imshape[1]*0.45, 1.2*imshape[0]/2),
                        (imshape[1]*0.55, 1.2*imshape[0]/2),
                        (imshape[1]*0.93,imshape[0])]], dtype=np.int32)
masked_edges = region_of_interest(edges, vertices)
```
Note that I am using the image shape to define the region of interest. This way, the mask becomes more robust to different image resolution (although not necessary to this dataset).

---
#### Hough lines detection

After having the edges, is necessary to detect the lines. To do this I use Hough Lines Transform wrapped by one helper function.

```python
# Define the Hough transform parameters
rho = 1             # distance resolution in pixels of the Hough grid
theta = 6*np.pi/180 # angular resolution in radians of the Hough grid
threshold = 7       # minimum number of votes (intersections in Hough grid cell)
min_line_len = 10   # minimum number of pixels making up a line
max_line_gap = 12   # maximum gap in pixels between connectable line segments

# Run Hough on edge detected image
lines = hough_lines(masked_edges, rho, theta, threshold, min_line_len, max_line_gap)
```

---
#### Drawing the lines

With the detected lines is necessary to draw them, but not drowing the lines the way that comes from Hough lines detection. To improve the accuracy of the detector we consider all the points in the lines of the lane line to fit the best possible line. To achieve this we first separate the lines by right and left lane lines by verifying at each side the X coordinate is. Also, each line is just separated if the aboslute value of its angular coeficient is higher than 0.5 (a line less than 31 degrees).

Further, with the lines separated and filtered the best linear fit is calculated using linear regression `numpy.polyfit` and then drawing it. When the `draw_lines` function is called with `draw_variance = True` the variance of the regression is used to plot other 2 lines with its linear coefficient increased or decreased by one standard deviation. The default is `draw_variance = False` just drawing a line with the best fitted value.

The code is as below:
```python
# Draw the lines on the original image
line_img = np.zeros((masked_edges.shape[0], masked_edges.shape[1], 3), dtype=np.uint8)
draw_lines(line_img, lines)
line_image = region_of_interest(line_image, vertices)    
result = weighted_img(line_image, image)
```

An example of the output when drawing lines with the variance is shown below:

<img src="./test_images_output_var/solidWhiteCurve.jpg"> 

    
## 2. Potential shortcomings

This code is not perfect and have potential shortcomings. First of all is the color segmentation, although the colors thresholds has a slack it may fail to detect a line in extreme conditions. Also, others cars getting into the region of interest or weird marks on the road may pass the color filter and have lines with angle greater than 31 degrees making the detection much wronger. In that case the standard deviation (or the variance) could be used to at least reject that line.

Also, the parameters are hard coded to work with the test dataset. An environment much different than the dataset one can make troubles. Ideally the parameters should be adaptive to the environment or based on a huge dataset. This codes fit just straight lines, and may fail with high curves.


## 3. Possible improvements

The possibles improvements may come in an attempt to prevent the potential shortcomings. So, the color segmentation should be adaptative. The region of interest could also be located by detecting where the road is. With the segmented image having more contours than just the lane lines some filters could be applyied to reject countours that are not lane lines. This could be achieved iteration over contours and verifying ratios, shape, etc.

Also to curves is possible to rough lines fail to detect the lines, so a better aproach would be to find the corners of the lanes or even making the contour skelethon and sampling points to later fit a quadratic function with those points. During this process I tried to fit a quadratic function but with few points the fit was bad. 
